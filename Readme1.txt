variables:
    MAVEN_OPTS: "-Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true -Dmaven.wagon.http.ssl.insecure=true -Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
    MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
    POSTGRES_USER: "postgres"
    POSTGRES_PASSWORD: "postgres"
    POSTGRES_DB: "atlante"
    SONAR_URL_DEV: https://sonar.cip-desenv.net:8443/
    SONAR_URL_PRD: https://sonar.cip.lan.com/
    BUILD_TARGET: "src/"
    NEW_VERSION: "1.0.1"
    MERGE_RESULT: "true"
    SONAR_SSL_CERTIFICATE: "/tools/certificado_sonar_desenv.cer"
  
analyze:
    stage: analyze
    image: 
      name: nexus.cip.lan.com:50000/devsecops/jenkins-node-devsecops:1.0.12
      entrypoint: [""]
    #image: "nexus.cip.lan.com:50000/devsecops/cip-devsecops-cicd:1.0.2 /bin/bash"
    services:
      - name: nexus.cip.lan.com:50000/cip-core/desenv/postgres-dup:1.0.0
     # - name: nexus.cip.lan.com:50008/mongo
        alias: dbpostgres
        command: ["postgres", "--max_prepared_transactions=1000"]
    #volumes:
    #  - /tmp:/tmp/host
   
    cache:
      paths:
        - .m2/repository
    before_script: 
      - echo "Validando tecnologia informada."
      - | 
        shopt -s nocasematch; if [[ "$TECNOLOGIA" != "NPM" && "$TECNOLOGIA" != "JAVA" ]]; then 
            echo  "Tecnologia {$TECNOLOGIA} nao suportada ou nao informada, favor validar no arquivo gitlab-ci.yml do projeto" 
            exit 1
        fi   
      - git config --global user.email "${GITLAB_USER_EMAIL}" && git config --global user.name "${GITLAB_USER_NAME}"
      - git config --global merge.ours.driver true
      - cd /workspace;
      - echo "Clonando branch ${CI_COMMIT_BRANCH}"
      - git clone -b ${CI_COMMIT_BRANCH} https://gitlab-ci-token:${GITLAB_PUSH_TOKEN}@gitlab.cip.lan.com/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}.git
      - echo "git clone -b ${CI_COMMIT_BRANCH} https://gitlab-ci-token:${GITLAB_PUSH_TOKEN}@gitlab.cip.lan.com/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}.git"
      - cd ${CI_PROJECT_NAME}
    script: 
      - echo "Executando sonar"
      - |
        shopt -s nocasematch; if [[ "$TECNOLOGIA" == "JAVA" ]]; then
          echo "Iniciando Build java"
          ls -ltr /tools
          export PATH=/opt/apache-maven-3.6.3/bin:/tools/java-11-openjdk-amd64/bin:/node-v10.16.3-linux-x64/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
          export JAVA_HOME=/tools/jdk-11.0.12/
          mvn clean install -DskipFormModel=false -DskipTests
          echo "...Build success finished..."
          echo "...Iniciantos Testes..."    
          export PATH=/opt/apache-maven-3.6.3/bin:/tools/java-11-openjdk-amd64/bin:/node-v10.16.3-linux-x64/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
          export JAVA_HOME=/tools/java-11-openjdk-amd64/
          echo "...Verificando Quality Gate...  "
          curl -k https://sonar.cip.lan.com/api/qualitygates/project_status?projectKey=$ID_PROJETO_SONAR_S |grep status| cut -d'"' -f6  >> resutado_final.log
          echo "curl -k https://sonar.cip.lan.com/api/qualitygates/project_status?projectKey=$ID_PROJETO_SONAR_S"
          SONAR_RESULT=$(cat resutado_final.log)
          echo $SONAR_RESULT
        fi
      - |
        shopt -s nocasematch; if [[ "$TECNOLOGIA" == "NPM" ]]; then
          rm -rf resutado_final.log
          echo "Iniciando Build NPM" 
          cp -r /tools/.npmrc /root
          chmod +x /tools/node-v14.17.6-linux-x64/bin/*
          export PATH=/opt/apache-maven-3.6.3/bin:/tools/java-11-openjdk-amd64/bin:/node-v10.16.3-linux-x64/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
          export JAVA_HOME=/tools/java-11-openjdk-amd64/
          export NODEJS_HOME=/tools/node-v14.17.6-linux-x64
          export PATH=$NODEJS_HOME/bin:$PATH
          cd /builds/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}
          npm install -D jest jest-preset-angular @types/jest eslint-config-airbnb sonar-scanner --save-dev -verbose --ignore-scripts
          npm install --verbose
           ./build.sh
          echo "...Build success finished..."
          npm -version
          npm run test
          npm run coverage
          npm run sonar
          echo "...Consultando resultado do Quality Gate..."
          curl -k https://sonar.cip.lan.com/api/qualitygates/project_status?projectKey=$BRANCH_DESTINO |grep status| cut -d'"' -f6  >> resutado_final.log
          echo "curl -k https://sonar.cip.lan.com/api/qualitygates/project_status?projectKey=$BRANCH_DESTINO"
          SONAR_RESULT=$(cat resutado_final.log)
          echo $SONAR_RESULT
        fi
      - echo "Sonar Result = $SONAR_RESULT"
      - |
        shopt -s nocasematch; if [[ "$SONAR_RESULT" == "OK" ]]; then 
          cd /workspace/${CI_PROJECT_NAME}
          echo "Os testes passaram no Quality Gate, iniciando merge."
          echo ".gitlab-ci.yml merge=ours" >> .gitattributes 
          git remote update 
          git fetch 
          git checkout origin/${BRANCH_DESTINO}
          git merge -m "Merge realizado pelo usuario ${GITLAB_USER_NAME} " -X theirs --allow-unrelated-histories origin/${CI_COMMIT_BRANCH} 
          git push --force https://gitlab-ci-token:${GITLAB_PUSH_TOKEN}@gitlab.cip.lan.com/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}.git HEAD:${BRANCH_DESTINO} 
          MERGE_RESULT="true"
          echo " Merge realizado com sucesso entre as branches origem ${CI_COMMIT_BRANCH} destino ${BRANCH_DESTINO} "
          echo $TECNOLOGIA && echo $MERGE_RESULT         
        else 
          MERGE_RESULT="false"
          echo "Merge nao executados pois os testes nao passaram no quality Gate"
          exit 1
        fi
      - echo "Starting Build"
      # - |
      #     shopt -s nocasematch; if [[ "$MERGE_RESULT" == "true" ]]; then
      #       shopt -s nocasematch; if [[ "$TECNOLOGIA" == "JAVA" ]]; then
      #         echo "Iniciando Build java"
      #         ls -ltr /tools
      #         export PATH=/opt/apache-maven-3.6.3/bin:/tools/jdk-11.0.12/bin:/node-v10.16.3-linux-x64/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
      #         export JAVA_HOME=/tools/jdk-11.0.12/
      #         mvn clean install -DskipFormModel=false -DskipTests
      #         echo "...Build success finished..."
      #         exit 0
      #       fi
      #       shopt -s nocasematch; if [[ "$TECNOLOGIA" == "NPM" ]]; then 
      #         echo "Iniciando Build NPM" 
      #         cp -r /tools/.npmrc /home/jenkinsslv
      #         export NODEJS_HOME=/tools/node-v14.17.6-linux-x64
      #         export PATH=$NODEJS_HOME/bin:$PATH
      #         chmod +x /tools/node-v14.17.6-linux-x64/bin/*
      #         npm -version
      #         npm install --verbose
      #         ./build.sh
      #         echo "...Build success finished..."
      #         exit 0
      #       fi    
      #     else 
      #       echo "Build não realizado pois o merge nao foi realizado para a branch ${BRANCH_DESTINO}"
      #       exit 1
      #     fi
#####
    except: 
      - /^develop2/
    only: 
      - /^feature*/
  
