FROM tomcat:jdk11-temurin-focal
WORKDIR /app
COPY JavaWeb /app/
COPY entrypoint.sh /app/entrypoint.sh
CMD chmod +x /app/entrypoint.sh
CMD ["sh", "/app/entrypoint.sh"]
EXPOSE 8080
