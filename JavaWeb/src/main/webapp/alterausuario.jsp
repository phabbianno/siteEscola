<%@page import="pack.*,java.sql.*"%>
<jsp:useBean id="obj" class="pack.LoginBean"/>
<jsp:setProperty property="*" name="obj"/>
<%
String action = request.getParameter("action"); 
%>
	<!DOCTYPE html>
	<html lang="pt-BR">
	<head>
	<title>Lista de alunoss</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	</head>
<body>
<table class="center" border="1">
<tr class="center">
<form class="login-form" action="alterausuario.jsp" method="post"> 
<%
Connection con=DriverManager.getConnection(Provider.CONNECTION_URL,Provider.USERNAME,Provider.PASSWORD);    
Statement statement = con.createStatement();
String login_username = (String)session.getAttribute("login_username");
String login_pass = (String)session.getAttribute("login_pass");
int login_type = (int)session.getAttribute("login_type");
Validacao valid = new Validacao();

if ("alterar".equals(action)) {
	int tipo = Integer.parseInt(request.getParameter("tipo"));
	String valorTipo = "";
	if (tipo==1) valorTipo = "Administrador"; else valorTipo = "Usuario Comum";
	%> <center><h1> <% out.println(action.toUpperCase()); %> dados do usu&aacute;rio: </h1></center>
    <input type="text" hidden="hidden" name="ID_AL" value="<%= request.getParameter("ID_AL")%>" /><br />
    <input type="text" hidden="hidden" name="ID_USR" value="<%= request.getParameter("ID_USR")%>" /><br />
    <tr><td>RA: <input type="text" name="RA"  size="7" maxlength="7" value="<%= request.getParameter("RA")%>" /></td></tr>
    <tr><td>Nome: <input type="text" name="nome"  size="50" maxlength="255" value="<%= request.getParameter("Nome")%>" /></td></tr>
    <tr><td>E-mail: <input type="text" name="email"  size="50" maxlength="255" value="<%= request.getParameter("email")%>" /></td></tr>
    <tr><td>Senha: <input type="password" name="pass"  size="50" maxlength="255" value="<%= request.getParameter("pass")%>" /></td></tr>
    <% if (login_type==1){ %>
    <tr><td><label for="tipo">Tipo de Conta:</label>
		<select name="tipo" id="tipo">
		  <option <% if (tipo==1){%> selected="selected"<%} %>value="1">Administrador</option>
		  <option <% if (tipo==2){%> selected="selected"<%} %>value="2">Usu&aacute;rio Comum</option>
		</select></td></tr>
	<% } else { %>
	Tipo de Conta: <input type="hidden" name="tipo" value="<%= request.getParameter("tipo")%>" /><% out.println(valorTipo); %><br />
	<% } %>
    <br/>
    <br/>    
    <tr><td>
    <input type="submit" value="Alterar Usuario" name="action" style="background-image: url('images/botao-blue.png'); border:none; background-repeat:no-repeat;background-size:100% 100%;">
    <tr><td> <input type="button" value="Voltar" onClick="history.go(-1)">  </td></tr>
    </td></tr>
 
    <%
} else if ("excluir".equals(action)) {
	%><center><h1>Confirma a a&ccedil;&atilde;o de <% out.println(action.toUpperCase());%> usu&aacute;rio ? </h1></center>
	<input type="text" hidden="hidden" name="ID_AL" value="<%= request.getParameter("ID_AL")%>" /><br />
	<input type="text" hidden="hidden" name="ID_USR" value="<%= request.getParameter("ID_USR")%>" /><br />
    <tr><td>RA: <input type="hidden" name="RA" value="<%= request.getParameter("RA")%>" /><%= request.getParameter("RA")%> </td></tr>
    <tr><td>Nome: <input type="hidden" name="nome" value="<%= request.getParameter("Nome")%>" /><%= request.getParameter("Nome")%> </td></tr>
    <tr><td>E-mail: <input type="hidden" name="email" value="<%= request.getParameter("email")%>" /><%= request.getParameter("email")%></td></tr>
    <tr><td>Tipo de Conta: <input type="hidden" name="tipo" value="<%= request.getParameter("tipo")%>" /><%= request.getParameter("tipo")%></td></tr>
    <br/>
    <br/>
    <tr><td><input type="submit" value="Excluir Usuario" name="action" style="background-image: url('images/botao-blue.png'); border:none; background-repeat:no-repeat;background-size:100% 100%;"></td></tr>
    <tr><td> <input type="button" value="Voltar" onClick="history.go(-1)">  </td></tr>
    <%
} else if ("adicionar novo usuario".equals(action)) {
	%><center><h1><%out.println(action.toUpperCase());%> dados do usu&aacute;rio: </h1></center>
	<tr><td>RA: <input type="text" name="RA" size="7" maxlength="7" value=""/></td></tr>
    <tr><td>Nome: <input type="text" name="nome" size="50" maxlength="255" value="" /></td></tr>
    <tr><td>E-mail: <input type="text" name="email" size="50" maxlength="255" value="" /></td></tr>
    <tr><td>Senha: <input type="password" name="senha" maxlength="255" value="" /></td></tr>
    <tr><td><label for="tipo">Tipo de Conta:</label>
		<select name="tipo" id="tipo">
		  <option value="1">Administrador</option>
		  <option value="2">Usu&aacute;rio Comum</option>
	</select>
    </td></tr>
    <br/>
    <br/>
    <tr><td><input type="submit" value="Adicionar Usuario" name="action" style="background-image: url('images/botao-blue.png'); border:none; background-repeat:no-repeat;background-size:100% 100%;"></td></tr>
    <tr><td> <input type="button" value="Voltar" onClick="history.go(-1)">  </td></tr>
<%
} 
%>
</form>
<form class="login-form" action="loginprocess.jsp" method="post"> <%

if ("Adicionar Usuario".equals(action)) {
	String RA = request.getParameter("RA");
	String Nome = request.getParameter("nome");
	String Email = request.getParameter("email");
	String Senha = request.getParameter("senha");
	String Tipo = request.getParameter("tipo");
	String val = valid.validacamposADDUser(RA, Nome, Email, Senha, Tipo);
	int ID_AL = 0;
	int ID_USR = 0;
	if (val.equals("")){
		try {
			ResultSet resultSetID_AL = statement.executeQuery("Select ID_AL from alunos order by ID_AL desc LIMIT 1");
			if(resultSetID_AL.next()) {
				ID_AL = resultSetID_AL.getInt("ID_AL") + 1;
			}
			ResultSet resultSetID_USR = statement.executeQuery("Select ID_USR from usuarios order by ID_USR desc LIMIT 1");
			if(resultSetID_USR.next()) {
				ID_USR = resultSetID_USR.getInt("ID_USR") + 1;
			}
			
			int i = statement.executeUpdate("INSERT INTO usuarios (ID_USR, Login, Senha, Tipo) VALUES (\""+ID_AL+"\", \""+RA+"\",\""+Senha+"\",\""+Tipo+"\")");
			int j = statement.executeUpdate("INSERT INTO alunos (ID_AL, RA, Nome, email) VALUES (\""+ID_USR+"\",\""+RA+"\",\""+Nome+"\",\""+Email+"\")");
			if (i==1){
				%><tr><td> <%out.println("Usu&aacute;rio "+RA+" adicionado com sucesso!");%></td></tr><%
			}else{
				%><tr><td> <%out.println("Usu&aacute;rio "+RA+" n&atilde;o foi adicionado! Contacte o administrador do sistema !");%></font></td></tr><%
			}
		
			%>		
			<input type="text" hidden="hidden" name="username" value="<%out.println(login_username); %>">
			<input type="password" hidden="hidden" name="pass" value="<%out.println(login_pass);%>">
			<br/>
	        <tr><td><input type="submit" value="Click aqui para continuar..." /></td></tr>
			
			<%
			
		} catch (Exception e) {
			System.out.print(e);
			e.printStackTrace();
		} 
	}else{
		%>
		<tr><td><FONT COLOR="red"> <% out.println(val); %></td></tr>
		</FONT>		
		<form class="login-form" action="loginprocess.jsp" method="post"> 
		<input type="text" hidden="hidden" name="username" value="<%out.println(login_username); %>">
		<input type="password" hidden="hidden" name="pass" value="<%out.println(login_pass);%>">
		<br/>
        <tr><td><input type="submit" value="Click aqui para acessar a lista de alunos novamente..." /></td></tr>
		</form>
		<%
	}
	
} else if ("Excluir Usuario".equals(action)) { 
	int ID_AL = Integer.parseInt(request.getParameter("ID_AL"));
	int ID_USR = Integer.parseInt(request.getParameter("ID_USR"));
	String RA = request.getParameter("RA");
	RA = RA.replaceAll("\\r\\n|\\r|\\n", "");
	if (!RA.equalsIgnoreCase(login_username)){
		try {
			int i = statement.executeUpdate("Delete from alunos where ID_AL ="+ID_AL+"");
			int j = statement.executeUpdate("Delete from usuarios where ID_USR ="+ID_USR+"");
			if (i+j==2){
				%><tr><td> <%out.println("Usu&aacute;rio "+RA+" excluído com sucesso!");%></td></tr><%
			}else{
				%><tr><td><font color="red"> <%out.println("Usu&aacute;rio "+RA+" n&atilde;o foi excluído! Contacte o administrador do sistema !");%></font></td></tr><%
			}
					
			%>		
			<input type="text" hidden="hidden" name="username" value="<%out.println(login_username); %>">
			<input type="password" hidden="hidden" name="pass" value="<%out.println(login_pass);%>">
	        <br/>
	        <tr><td><input type="submit" value="Click aqui para continuar..." /></td></tr>
			
			<%
			
		} catch (Exception e) {
			System.out.print(e);
			e.printStackTrace();
		}
	}else{
		%>
		<tr><td><FONT COLOR="red"> N&atilde;o é possível excluir o usu&aacute;rio que est&aacute; logado no sistema!</FONT></td></tr>						
		<form class="login-form" action="loginprocess.jsp" method="post"> 
		<input type="text" hidden="hidden" name="username" value="<%out.println(login_username); %>">
		<input type="password" hidden="hidden" name="pass" value="<%out.println(login_pass);%>">
		<br/>
        <tr><td><input type="submit" value="Click aqui para acessar a lista de alunos novamente..." /></td></tr>
		</form>
		<%
	}

} else if ("Alterar Usuario".equals(action)) {
	int ID_AL = Integer.parseInt(request.getParameter("ID_AL"));
	int ID_USR = Integer.parseInt(request.getParameter("ID_USR"));
	String RA = request.getParameter("RA");
	String Nome = request.getParameter("nome");
	String Email = request.getParameter("email");
	String Senha = request.getParameter("pass");
	String Tipo = request.getParameter("tipo");
	try {
		int i = statement.executeUpdate("Update alunos set RA = \""+RA+"\", Nome = \""+Nome+"\", email = \""+Email+"\" where ID_AL ="+ID_AL+"");
		int j = statement.executeUpdate("Update usuarios set Login = \""+RA+"\", Senha = \""+Senha+"\", Tipo = "+Tipo+" where ID_USR ="+ID_USR+"");
		if (i+j==2){
			%><tr><td> <% out.println("Usu&aacute;rio "+RA+" atualizado com sucesso!"); %></td></tr><%
		}else{
			%><tr><td><font color="red"> <% out.println("Usu&aacute;rio "+RA+" n&atilde;o foi atualizado! Contacte o administrador do sistema !"); %></font></td></tr><%
		}
		login_username = (String)session.getAttribute("login_username");
		login_pass = (String)session.getAttribute("login_pass");		
		%>		
		<input type="text" hidden="hidden" name="username" value="<%out.println(login_username); %>">
		<input type="password" hidden="hidden" name="pass" value="<%out.println(login_pass);%>">
        <br/>
        <tr><td><input type="submit" value="Click aqui para continuar..." /></td></tr>
		
		<%
		
	} catch (Exception e) {
		System.out.print(e);
		e.printStackTrace();
	}
}
%>
     </form>     
     	<tr>
 		<form class="login-form" action="logout.jsp" method="post">
		<td align="center"><input type="image" src="images/logout.png" alt="Submit" width="68" height="28" value="Sair" alt="Sair">  </td> 
		</form>
 		</tr>
   </table>
   </body>
</html>
